<!doctype html>
<html lang="pt-br">
  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aula de Laravel</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/estilos.css') }}">
</head>
    

<body>
    <div class="container-md">
        <h1>Baterias Laravel</h1>
        <hr>
        <nav>
            <ul>
                <li><a href="{{ route('/')}}">inicio</a></li>
                <li><a href="{{ route('teoria')}}">teoria</a></li>
                <li><a href="{{ route('procedimento')}}">procedimento</a></li>
                <li><a href="{{ route('medicoes')}}">medições</a></li>
                <li><a href="{{ route('conclusoes')}}">conclusões</a></li>
            </ul>
        </nav>
       
        
        @yield("conteudo")
        <div class="rodape container-md">
            @yield("rodape")
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</body>

 </html>