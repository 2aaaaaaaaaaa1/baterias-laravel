 <main>
        <h1>Medições</h1>
        <hr>
        <h2>Valores obtidos:</h2>
        <table class="table table-striped table-bordered" id="tbDados">
            <thead>
                <tr>
                    <th>Pilha/Bateria</th>
                    <th>Tensão nominal (V)</th>
                    <th>Capacidade de corrente (mA.h)</th>
                    <th>Tensão sem carga(V)</th>
                    <th>Tensão com carga(V)</th>
                    <th>Resitência de carga(ohm)</th>
                    <th>Resistência interna(ohm)</th>
                </tr>
            </thead>
           <tbody>
                <tr>
                  <td>Pilha Alcalina Duracel AA</td>
                    <td>1,5</td>
              <td>2800mA.h</td>
                    <td>1,304</td>
                    <td>1,286</td>
                    <td>23,7</td>
                    <td>0,3317</td>
                </tr>
            </tbody>
        </table>
    </main>
    @section('rodape')
        <h4>Rodapé Teoria</h4>
    @endsection